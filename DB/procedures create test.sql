call create_entry('test entry','test desc',404,1,1,1,-1);;
call create_entry_basic('basic entry','basic desc',-1);
select * from "Entry";

--delete from "Entry";

call create_unit('test unit', 'test unit desc',-1);
select * from "Units";

call create_location('test main location', 'test location desc',1,-1);
call create_location_basic('test 2nd location', 'test 2nd location desc',1);
select * from "Locations";

call create_photo_basic('re',1,-1);
select * from "Photos";


call create_Tag('test2 tag',124,-1);
select * from "EntryTags";

call create_category('test category 2 ',125,-1);
select * from "Categories";

call create_tag_junction(2,2,-1);
select * from "Entry_EntryTags";

call create_category_junction(2,1,-1);
select * from "Entry_Categories";

call create_photo_storage('testfilename.png',-1);
select * from "Photo_storage";

call create_photo_junction(1,1,-1);
select * from "Photo_storage_Photos";






-- drop schema if exists public cascade;
create schema if not exists public; 

select * from "Entry_Categories" order by "Entry_id";
select * from "Categories";
select * from "Entry_EntryTags" order by "Entry_id";

--delete from "Entry_EntryTags" where id=1

select * from "EntryTags";
select * from "Units";
select * from "Photos";
select * from "Flags";

select * from "Entry" order by "id";
select * from "Locations";

call update_entry_name(1,'new name',1)
call update_entry_edit_info(1,2,1)
call update_entry_place(1,3,1)
call update_entry_unit(1,2,1)
call update_entry_count(1,501,1)
call update_entry_description(1,'new description',1)
call update_entry_photo(1,2,1)

call update_photos(1,0,'0 index test image.png','text image thumb.png',1,1)

--drop procedure create_entr

CREATE OR REPLACE PROCEDURE create_entry(
	entry_name text,
	entry_desc text, 
	entry_count int,
	entry_count_unit int,
	entry_location int,
	entry_photo int,
	change_by_user_id int
)
language sql
as $$
	insert into "Entry"(
		"name",
		"description",
		"photo",
		"count",
		"count_unit",
		"location"
	)
	values (
		entry_name,
    	entry_desc,
		entry_photo,
		entry_count,
		entry_count_unit,
		entry_location	
	);
$$;

CREATE OR REPLACE PROCEDURE create_entry_basic(
	entry_name text,
	entry_desc text, 
	change_by_user_id int
)
language sql
as $$
	insert into "Entry"(
		"name",
		"description",
		"photo",
		"count",
		"count_unit",
		"location"
	)
	values (
		entry_name,
   		entry_desc,
		1,
		1,
		1,
		1
	);
$$;


CREATE OR REPLACE PROCEDURE create_unit(
	units_unit text,
	units_desc text,
	change_by_user_id int
)
language sql
as $$
	insert into "Units"(
		"unit",
		"description"
	)
	values (
		units_unit,
  		units_desc
	);
$$;

CREATE OR REPLACE PROCEDURE create_location(
	loc_name text,
	loc_desc text,
	loc_parent int,
	change_by_user_id int
)
language sql
as $$
	insert into "Locations"(
		"name",
		"description",
		"location"
	)
	values (
		loc_name,
    	loc_desc,
		loc_parent
	);
$$;

CREATE OR REPLACE PROCEDURE create_location_basic(
	loc_name text,
	loc_desc text,
	change_by_user_id int
)
language sql
as $$
	insert into "Locations"(
		"name",
		"description"
	)
	values (
		loc_name,
    	loc_desc
	);
$$;



CREATE OR REPLACE PROCEDURE create_Tag(
	tag_name text,
	tag_color int,
	change_by_user_id int
)
language sql
as $$
	insert into "EntryTags"(
		"Tag",
		"color"
	)
	values (
		tag_name,
    	tag_color
	);
$$;


CREATE OR REPLACE PROCEDURE create_category(
	category_name text,
	category_color int,
	change_by_user_id int
)
language sql
as $$
	insert into "Categories"(
		"category",
		"color"
	)
	values (
		category_name,
    	category_color
	);
$$;

CREATE OR REPLACE PROCEDURE create_category_junction(
	entry_id int,
	category_id int,
	change_by_user_id int
)
language sql
as $$
	insert into "Entry_Categories"(
		"Entry_id",
		"Category_id"
	)
	values (
		entry_id,
    	category_id
	);
$$;


CREATE OR REPLACE PROCEDURE create_tag_junction(
	entry_id int,
	tag_id int,
	change_by_user_id int
)
language sql
as $$
	insert into "Entry_EntryTags"(
		"Entry_id",
		"Tag_id"
	)
	values (
		entry_id,
    	tag_id
	);
$$;


CREATE OR REPLACE PROCEDURE create_photo_basic(
	photo_thumbnail text,
	photo_container_id int,
	change_by_user_id int
)
language sql
as $$
	insert into "Photos"(
		"thumbnail_name",
		"container_id"
	)
	values (
		photo_thumbnail,
    	photo_container_id
	);
$$;


CREATE OR REPLACE PROCEDURE create_photo_junction(
	photo_id int,
	photo_storage_id int,
	change_by_user_id int
)
language sql
as $$
	insert into "Photo_storage_Photos"(
		"Photo_storage_id",
		"Photos_id"
	)
	values (
		photo_id,
    	photo_storage_id
	);
$$;

CREATE OR REPLACE PROCEDURE create_photo_storage(
	photo_file_name text,
	change_by_user_id int
)
language sql
as $$
	insert into "Photo_storage"(
		"photo_file_name"
	)
	values (
		photo_file_name
	);
$$;












								
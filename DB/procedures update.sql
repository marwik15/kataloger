--drop procedure update_photos


CREATE OR REPLACE PROCEDURE update_entry_name(
	e_id integer,
	new_text text, 
	change_by_user_id int
)
language sql
as $$
	update "Entry"
	set "name" = (new_text)
	where "Entry".id = e_id
$$;
	
-- not tested
CREATE OR REPLACE PROCEDURE update_entry_edit_info(
	e_id integer,
	new_edit_info integer,
	change_by_user_id int
)
language sql
as $$
	update "Entry"
	set "edit_info" = (new_edit_info)
	where "Entry".id = e_id
$$;


CREATE OR REPLACE PROCEDURE update_entry_location(
	e_id integer,
	new_location integer,
	change_by_user_id int
)
language sql
as $$
	update "Entry"
	set "location" = (new_location)
	where "Entry".id = e_id
$$;
								
CREATE OR REPLACE PROCEDURE update_entry_unit(
	e_id integer,
	new_unit integer,
	change_by_user_id int
)
language sql
as $$
	update "Entry"
	set "count_unit" = (new_unit)
	where "Entry".id = e_id
$$;

								
CREATE OR REPLACE PROCEDURE update_entry_description(
	e_id integer,
	new_description text,
	change_by_user_id int)
language sql
as $$
	update "Entry"
	set "description" = (new_description)
	where "Entry".id = e_id
$$;

-- not tested
CREATE OR REPLACE PROCEDURE update_entry_photo(
	e_id integer,
	new_photo integer,
	change_by_user_id int
)
language sql
as $$
	update "Entry"
	set "photo" = (new_photo)
	where "Entry".id = e_id
$$;


CREATE OR REPLACE PROCEDURE update_photos(
	p_id integer,
	new_thumbnail_name text,
	new_container_id integer,
	change_by_user_id int
)
language sql
as $$
	update "Photos"
	set "thumbnail_name" = (new_thumbnail_name),
	"container_id" = (new_container_id)
	where "Photos".id = p_id
$$;


								
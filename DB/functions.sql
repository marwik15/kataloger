---function

CREATE OR REPLACE FUNCTION get_tags_id(entry_id integer) RETURNS table(t_id integer)
    AS $$ 
	SELECT "Tag_id" from "Entry_EntryTags" where "Entry_id"=(entry_id) order by "Tag_id"
	$$
    LANGUAGE SQL;
	
select * from get_tags_id(5)

select count(*) from get_tags_id(5)



CREATE OR REPLACE FUNCTION get_tags_name(t_id integer) RETURNS table(t_name VARCHAR)
	as $$
	select "Tag" from "EntryTags" et
	inner join get_tags_id(t_id) tag_id
		on et.id=tag_id
	$$
	language sql;


select "Tag" from "EntryTags" et
inner join get_tags_id(1) tag_id
	on et.id=tag_id
	
select * from get_tags_name(5)


-------------------

/*CREATE OR REPLACE FUNCTION increment(i integer) RETURNS integer AS $$
        BEGIN
                RETURN i + 1;
        END;
$$ LANGUAGE plpgsql;

select * from increment(2)
*/
---


CREATE OR REPLACE FUNCTION get_entry_category_count(entry_id integer,out  category_count integer) 
    AS $$ 
	select COUNT("Entry_id") from "Entry_Categories" et where "Entry_id" = (entry_id)
	$$
    LANGUAGE SQL;

select * from get_entry_category_count(5);
select * from get_entry_tag_count(5);



select COUNT("Entry_id") from "Entry_EntryTags" et where "Entry_id" = 5;

CREATE OR REPLACE FUNCTION get_entry_tag_count(entry_id integer,out  tag_count integer) 
    AS $$ 
	select COUNT("Entry_id") from "Entry_EntryTags" et where "Entry_id" = (entry_id)
	$$
    LANGUAGE SQL;

select COUNT("Entry_id") from "Entry_Categories" et where "Entry_id" = 5
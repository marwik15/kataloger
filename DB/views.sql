
-----  entry
--drop view v_all_entries


create or replace view v_all_entries
as
select e.id as "entry_id", e.name, e.description, 
e.photo , l.name as "location", e.count,eet."Category_id",cat.category, u.id as unit_id, u.unit  
from public."Entry" e
	inner join public."Units" u
		on u.id=e."count_unit"
	inner join public."Locations" l
		on l.id=e.location
	left join public."Entry_Categories" eet
		on eet."Entry_id" = e.id
	left join public."Categories" cat
		on cat.id = eet."Category_id"
order by e.id;

select * from v_all_entries;

select * from "Entry";
select * from "Entry_Categories";
select * from "Categories";

---
---final brief
create or replace view v_all_entry_brief 
as
select e.id as "entry_id", e.name, e.description, 
e.photo ,  l.name as "location", e.count, u.id as unit_id, u.unit  
from 
	public."Entry" e
inner join public."Units" u
	on u.id=e."count_unit"
inner join public."Locations" l
	on l.id=e.location
order by e.id;
---
select * from v_all_entry_brief;

------------------------------------
create or replace view v_entry_tags_joined
as
select eet."Tag_id", et."Tag",e.name as entry_name, e.id as "entry_id" 
from "Entry_EntryTags" eet
	inner join public."EntryTags" et
		on  eet."Tag_id" = et."id"	
	inner join public."Entry" e
		on eet."Entry_id" = e.id
order by eet."Tag_id";
	
select * from v_entry_tags_joined;

-----------------------------------
create or replace view v_tags_id
as
select et."id", et."Tag" 
	from "EntryTags" et
order by et."id";

select * from v_tags_id;
--------------------

create or replace view v_entry_categories_joined
as
select ec."Entry_id", cat."category", e.name as entry_name, e.id as "entry_id" 
from "Entry_Categories" ec
	inner join public."Categories" cat
		on  ec."Category_id" = cat."id"	
	inner join public."Entry" e
		on ec."Entry_id" = e.id
order by ec."Category_id";

select * from v_entry_categories_joined;
	
select * from "Categories";



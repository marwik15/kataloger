#include <nana/gui.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/slider.hpp>

#define ASIO_STANDALONE

#ifdef _WIN32
    #define _WIN32_WINNT 0x0A00
#endif // _WIN32



#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <asio/ts/internet.hpp>

using asio::ip::tcp;



int maind() {
    // proudly stolen from https://think-async.com/Asio/asio-1.20.0/doc/asio/examples/cpp14_examples.html
        try {
       
            std::string path{ "/" }, server{ "127.0.0.1" };
            asio::ip::tcp::iostream s;

            // The entire sequence of I/O operations must complete within 60 seconds.
            // If an expiry occurs, the socket is automatically closed and the stream
            // becomes bad.
            s.expires_after(std::chrono::seconds(60));

            // Establish a connection to the server.
            s.connect(server, "http");
            if (!s) {
                std::cout << "Unable to connect: " << s.error().message() << "\n";
                return 1;
            }

            // Send the request. We specify the "Connection: close" header so that the
            // server will close the socket after transmitting the response. This will
            // allow us to treat all data up until the EOF as the content.


            s << "GET " << path << " HTTP/1.0\r\n";
            s << "Host: " << server << "\r\n";
            s << "Accept: */*\r\n";
            s << "Connection: close\r\n\r\n";

            // By default, the stream is tied with itself. This means that the stream
            // automatically flush the buffered output before attempting a read. It is
            // not necessary not explicitly flush the stream at this point.

            // Check that response is OK.
            std::string http_version;
            s >> http_version;
            unsigned int status_code;
            s >> status_code;
            std::string status_message;
            std::getline(s, status_message);
            if (!s || http_version.substr(0, 5) != "HTTP/") {
                std::cout << "Invalid response\n";
                return 1;
            }
            if (status_code != 200) {
                std::cout << "Response returned with status code " << status_code << "\n";
                return 1;
            }

            // Process the response headers, which are terminated by a blank line.
            std::string header;
            while (std::getline(s, header) && header != "\r")
                std::cout << header << "\n";
            std::cout << "\n";

            // Write the remaining data to output.
            std::cout << s.rdbuf();
        }
        catch (std::exception& e) {
            std::cout << "Exception: " << e.what() << "\n";
        }

        return 0;
}

#include <nana/gui.hpp>
#include <nana/gui/widgets/group.hpp>
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/form.hpp>
#include <nana/gui/widgets/label.hpp>
#include <iostream>
using namespace nana;
int main() {
    form fm{ API::make_center(600,400) };
    //fm.bgcolor(colors::mint_cream );
    place plc(fm);
    std::vector<std::unique_ptr<button>> btns;
    // the most external widgets
    label  out{ fm,  "This label is out of any group" };
    group  ext_gr{ fm,  "An external <bold=true, color=blue>Group:</>", true };
    plc.div("vert gap=10 margin=5 <lab weight=30><all> ");
    plc["lab"] << out;
    plc["all"] << ext_gr;
    // the external group contain:
    group grp_left{ ext_gr,  ("A new <bold=true, color=0xff0000, font=\"Consolas\">Group:</>"), true };
    group grp_right{ ext_gr,  ("A right <bold=true, color=0xff0000, font=\"Consolas\">Group:</>"), true };
    ext_gr.div("horizontal gap=3 margin=20  < <left_field> | 70% <right_field>> ");
    ext_gr["left_field"] << grp_left;
    ext_gr["right_field"] << grp_right;
    // the left group
    grp_left.div("buttons vert gap=5 margin=3");
    // the right group
    group nested(grp_right.handle());
    label  lab{ grp_right,  "A simple label " };
    button b1{ grp_right,  "add button" };
    b1.events().click([&grp_left, &btns] {
        btns.emplace_back(new button(grp_left, "Button"));
        grp_left["buttons"] << *btns.back();
        grp_left.collocate();
       
       
        std::cout << "button added\n";
    });

    button b2{ grp_right,  "button2" };
    button b3{ grp_right,  "button3" };

    b2.events().click([&grp_left, &btns] {
      
        //grp_left.erase(*btns[0]);
        btns.clear();
        grp_left.collocate();

        std::cout << "button removed\n";
     });

    grp_right.div("<vertical margin=2 gap= 2 <vert lab> | 40% < <left_field> | 70% <right_field>> >");
    grp_right["lab"] << lab.text_align(align::right) << nested;
    grp_right["left_field"] << b1;
    grp_right["right_field"] << b2 << b3;
    // the nested (rigth up) group
    label lab1{ nested,  "A very simple group:" };
    button b4{ nested,  "button4" };
    button b5{ nested,  "button5" };
    button b6{ nested,  "button6" };
    nested.div(" margin=3 min=30 gap= 2 all");
    nested["all"] << lab1 << b4 << b5 << b6;
    plc.collocate();
    //grp1.plc.collocate();    // OK
    fm.show();
    nana::exec();
}


int mainsadd() {
    using namespace nana;

    //Define a form.
    form fm;

    //Define a label and display a text.
    label lab{ fm, "Hello, <bold blue size=16>Nana C++ Library</>" };
    lab.format(true);

    //Define a button and answer the click event.
    button btn{ fm, "Quit" };
    btn.events().click([&fm] {
        fm.close();
    });

    slider slider(fm);
    slider.vertical(true);
 

    //Layout management
  

    fm.div("vert <><<><weight=80% text><<sl>>><><weight=24<><button><>><>");
    fm["text"] << lab;
    fm["button"] << btn;
    fm["sl"] << slider;
    fm.collocate();

    fm.zoom(true);

    //Show the form
    fm.show();

    //Start to event loop process, it blocks until the form is closed.
    exec();

    return 1;
}
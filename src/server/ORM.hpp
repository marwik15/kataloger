#pragma once

#include <fmt/core.h>
#include <fmt/format.h>

#include <iostream>

#include <vector>




class command {
public:

    command(std::string command_name,
            std::string query_prototype,
            std::vector<std::string> var_names) :
        commandName(command_name),
        query(query_prototype),
        var_names(var_names) {

       
    }

    std::string& getCommandName() {
        return commandName;
    }

    std::string& getQuery() {
        return finalQuery;
    }





    std::string commandName;
    std::string query;
    std::string finalQuery;
    std::vector<std::string> var_names;
    int params;

private:


    void makeQuery() {

        std::vector<std::string> args = { "42", "PANIC!","XD" };
        //std::string message = format_vector("The answer is {} so don't {} {}", args);

        //finalQuery = format_vector(query, args);
    }

};


class autoCommand {
public:
    std::string format_vector(std::string_view format,
                              std::vector<std::string> const& args) {
        using ctx = fmt::format_context;
        std::vector<fmt::basic_format_arg<ctx>> fmt_args;
        for (auto const& a : args) {
            fmt_args.push_back(
                fmt::detail::make_arg<ctx>(a));
        }

        return fmt::vformat(format,
                            fmt::basic_format_args<ctx>(
                                fmt_args.data(), fmt_args.size()));
    }

    autoCommand() {

        commands.push_back(command("create_basic_entry",
                                   "call create_entry_basic({},{},-1);",
                                   { "entry_name","entry_desc" }));
    }
    std::vector<command> commands;
private:
};
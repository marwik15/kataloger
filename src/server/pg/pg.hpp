
#pragma once

/*
 * src/test/examples/testlibpq.c
 *
 *
 * testlibpq.c
 *
 *      Test the C version of libpq, the PostgreSQL frontend library.
 */
#include <stdio.h>
#include <stdlib.h>
#include "libpq-fe.h"



class pgDB {

private:
    PGconn* conn;
    PGresult* res;

public:
    pgDB() {
        connect();
        begin();
    }

    bool sendCommand(std::string query) {
        
        res = PQexec(conn, query.c_str());
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            fprintf(stderr, "command failed: %s", PQerrorMessage(conn));
            PQclear(res);
            exit_nicely();
            return false;
        }

        PQclear(res);
        exit_nicely();
        return true;
    }

private:


    bool connect() {

        const char* conninfo;

        conninfo = "postgresql://postgres:admin@localhost:5433/kataLOGer?application_name=katalogerAgent";

        /* Make a connection to the database */
        conn = PQconnectdb(conninfo);

        /* Check to see that the backend connection was successfully made */
        if (PQstatus(conn) != CONNECTION_OK) {
            fprintf(stderr, "%s", PQerrorMessage(conn));
            exit_nicely();
            return false;
        }
        return true;
    }


   

    bool begin() {
        res = PQexec(conn, "BEGIN");
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            fprintf(stderr, "BEGIN command failed: %s", PQerrorMessage(conn));
            PQclear(res);
            exit_nicely();
            return false;
        }
        PQclear(res);
        return true;
    }

    void end() {
        /* end the transaction */
        res = PQexec(conn, "END");
        PQclear(res);
    }

    bool sendQuery() {
        // res = PQexec(conn, "select * from logger.\"Entry\"");
        res = PQexec(conn, "select * from public.\"Entry\"");

        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            fprintf(stderr, "FETCH ALL failed: %s", PQerrorMessage(conn));
            PQclear(res);
            exit_nicely();
            return false;
        }

        int  nFields;
        int   i, j;
        /* first, print out the attribute names */
        nFields = PQnfields(res);
        for (i = 0; i < nFields; i++) {
            auto re = PQfname(res, i);
            printf("%-15s", re);
        }
        printf("\n\n");

        /* next, print out the rows */
        for (i = 0; i < PQntuples(res); i++) {
            for (j = 0; j < nFields; j++) {
                auto re = PQgetvalue(res, i, j);
                printf("%-15s", re);
            }
            printf("\n");
        }

        PQclear(res);
        return true;
    }


    void exit_nicely(){
        end();
        PQfinish(conn);
    }

};



int test(int argc, char** argv) {
    /*pgDB pgConnect;

    pgConnect.connect();
    pgConnect.begin();

    pgConnect.sendQuery();

    pgConnect.end();
    pgConnect.exit_nicely();*/

    return 0;
}
